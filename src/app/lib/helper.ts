export function shuffle<T>(array: T[]) {
  let currentIndex = array.length;
  let temporaryValue: T = null;
  let randomIndex: number = null;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

export function toCategorical(emotion: number, numClasses: number) {
  let array = new Array<number>(numClasses);
  array = array.fill(0);
  if (emotion >= 0 && emotion < numClasses) {
    array[emotion] = 1;
  }
  return array;
}
