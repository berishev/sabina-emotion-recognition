import * as tf from '@tensorflow/tfjs';
import { toCategorical } from './helper';

const fileName = 'data';
const fileExtension = '.json';
const dataUrl = '/data/';
const IMAGE_SIZE = 2304;
export const ALL_DATA_SET = 72; // 72;
export const NUM_CLASSES = 7;

export enum NeuralEmotionTypeEnum {
  angry,
  disgust,
  fear,
  happy,
  sad,
  surprise,
  neutral
}

export enum NeuralUsageTypeEnum {
  training,
  publicTest
}

export interface INeuralItem {
  emotionType: NeuralEmotionTypeEnum;
  pixels: number[];
  usage: NeuralUsageTypeEnum;
}

class ParserPatitial {
  url: string = null;
  fetched: boolean = false;
  private _items: INeuralItem[] = null;

  // Deep cloned loaded array
  get items() {
    if (!this._items) return [];
    return JSON.parse(JSON.stringify(this._items)) as INeuralItem[];
  }

  // Deep cloned normalized array
  get itemsNormalized() {
    let items = this.items;
    return items.map(m => {
      m.pixels = m.pixels.map(k => k / 255);
      return m;
    });
  }

  get train() {
    return this.itemsNormalized.filter(m => m.usage == NeuralUsageTypeEnum.training);
  }

  get test() {
    return this.itemsNormalized.filter(m => m.usage == NeuralUsageTypeEnum.publicTest);
  }

  constructor(url: string) {
    this.url = url;
  }

  async load() {
    let res = await fetch(this.url);
    this._items = (await res.json()) as INeuralItem[];
    this.fetched = true;
    return this.items;
  }

  nextTrainBatch(batchSize: number, offset: number) {
    return this.nextBatch(batchSize, offset, this.train);
  }

  nextTestBatch(batchSize: number, offset: number) {
    return this.nextBatch(batchSize, offset, this.test);
  }

  nextBatch(batchSize: number, offset: number, data: INeuralItem[]) {
    const batchPixels: number[][] = [];
    const batchEmotions: number[][] = [];

    let offsetData = data.slice(offset, offset + batchSize);

    for (let item of offsetData) {
      let { emotionType, pixels } = item;
      let emotion = toCategorical(emotionType, NUM_CLASSES);
      batchPixels.push(pixels);
      batchEmotions.push(emotion);
    }

    const batchPixelsTensor = tf.tensor2d(batchPixels, [batchPixels.length, (batchPixels[0] && batchPixels[0].length) || IMAGE_SIZE]);
    const batchEmotionsTensor = tf.tensor2d(batchEmotions, [batchEmotions.length, (batchEmotions[0] && batchEmotions[0].length) || NUM_CLASSES]);

    return { batchPixelsTensor, batchEmotionsTensor };
  }
}

export default class ParserController {
  urls = new Array<number>(ALL_DATA_SET).fill(0).map((m, i) => `${dataUrl}${fileName}${i}${fileExtension}`);

  private cache: { [url: string]: ParserPatitial } = {};

  getPartitial(index: number) {
    let exists = this.existsIndex(index);
    if (!exists) return null;
    let url = this.urls[index];
    if (!this.cache[url]) this.cache[url] = new ParserPatitial(url);
    return this.cache[url];
  }

  existsIndex(index: number) {
    let url = this.urls[index];
    if (url) return true;
    return false;
  }

  get lenght() {
    let parts = this.getCachedPartitials();
    let lP = parts.map(m => m.items.length);
    let count = 0;
    for (let item of lP) {
      count += item;
    }
    return count;
  }

  getCachedPartitials() {
    return Object.keys(this.cache).map(m => this.cache[m]);
  }
}
