import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './app/App';
import Test from './app/Test';
import './index.css';

enum ShowItem {
  none,
  train,
  app
}

interface IState {
  show: ShowItem;
}

class Root extends React.Component<any, IState> {
  constructor(props) {
    super(props);
    this.state = {
      show: ShowItem.none
    };
  }

  render() {
    let { show } = this.state;
    if (show == ShowItem.app) return <Test />;
    if (show == ShowItem.train) return <App />;
    return (
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: '100vh', width: '100vw' }}>
        <button style={{ flex: 1, height: '100%', padding: 5, fontSize: 64 }} onClick={() => this.setState({ show: ShowItem.train })}>
          Train
        </button>
        <button style={{ flex: 1, height: '100%', padding: 5, fontSize: 64 }} onClick={() => this.setState({ show: ShowItem.app })}>
          App
        </button>
      </div>
    );
  }
}

ReactDOM.render(<Root />, document.getElementById('root') as HTMLElement);
